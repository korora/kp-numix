# numix-icon-theme
%global commit0 763489fcf4161796fc30c68e0c1819171b2a3c70
# numix-icon-theme-circle
%global commit1 7250a0618e6e18092c8b15df9f02ce7ac289e334
# Numix gtk theme
%global commit2 2e58680a210ccfd050b2a713c29f013fc34ab311
# Numix KDE theme
%global commit3 241c7bfb192f8fc72223f142ea98491ce759f2b3

%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global shortcommit1 %(c=%{commit1}; echo ${c:0:7})
%global shortcommit2 %(c=%{commit2}; echo ${c:0:7})
%global shortcommit3 %(c=%{commit3}; echo ${c:0:7})

Name:		numix
Version:	1807
Release:	17.git%{shortcommit0}%{dist}
Summary:	Numix Project

Source0:	https://github.com/numixproject/numix-icon-theme/archive/%{commit0}.tar.gz#/numix-icon-theme-%{shortcommit0}.tar.gz
Source1:	https://github.com/numixproject/numix-icon-theme-circle/archive/%{commit1}.tar.gz#/numix-icon-theme-circle-%{shortcommit1}.tar.gz
Source2:	https://github.com/numixproject/numix-gtk-theme/archive/%{commit2}.tar.gz#/numix-gtk-theme-%{shortcommit2}.tar.gz
Source3:        https://github.com/numixproject/numix-kde-theme/archive/%{commit3}.tar.gz#/numix-kde-theme-%{shortcommit3}.tar.gz

Group:		User Interface/Desktops
License:	GPLv3
URL:		http://numixproject.org

BuildArch:	noarch

%description
Numix is the official icon theme from the Numix project.
It is heavily inspired by, and based upon parts of the Elementary, Humanity and Gnome icon themes

%package icon-theme
Group:		User Interface/Desktops
Summary:	Numix Icons
Obsoletes:	korora-icon-theme-base
Provides:	korora-icon-theme-base
%description icon-theme
Numix is the official icon theme from the Numix project. 
It is heavily inspired by, and based upon parts of the Elementary, Humanity and Gnome icon themes

%package icon-theme-blue
Group:          User Interface/Desktops
Summary:        Numix Icons
%description icon-theme-blue
Numix is the official icon theme from the Numix project.
It is heavily inspired by, and based upon parts of the Elementary, Humanity and Gnome icon themes
This version of the Numix theme uses blue folders

%package icon-theme-circle
Group:		User Interface/Desktops
Summary:	Numix Circle Icons
Version:        1807
Release:        17.git%{shortcommit0}%{dist}
Obsoletes:	korora-icon-theme
Provides:	korora-icon-theme
%description icon-theme-circle
Circle is an icon theme for Linux from the Numix project

%package icon-theme-circle-blue
Group:          User Interface/Desktops
Summary:        Numix Circle Icons in Blue
Version:        1807
Release:        17.git%{shortcommit0}%{dist}
Obsoletes:	%{name}-icon-theme-circle-arc
%description icon-theme-circle-blue
Circle is an icon theme for Linux from the Numix project. This version uses blue folders.

%package gtk-theme
Group:		User Interface/Desktops
Summary:	Numix Gtk Theme
Version:    	2.6.7
BuildRequires:	rubygem-sass gdk-pixbuf2-devel
Requires:	gtk-murrine-engine
%description gtk-theme
Numix is a modern flat theme with a combination of light and dark elements. It supports Gnome, Unity, XFCE and Openbox.

%package kde-theme
Group:          User Interface/Desktops
Summary:        Numix KDE Theme
Version:        0.1.0
%description kde-theme
Numix KDE Theme is a modern flat theme with a combination of light and dark elements.

%prep
%setup -q -c %{name}-%{version}-%{release} -T -a 0
%setup -q -c %{name}-%{version}-%{release} -D -T -a 1
%setup -q -c %{name}-%{version}-%{release} -D -T -a 2
%setup -q -c %{name}-%{version}-%{release} -D -T -a 3

%build

%install
cd numix-gtk-theme-%{commit2}
%{make_install}
cd ..

install -d %{buildroot}%{_datadir}/icons

mkdir -p %{buildroot}%{_datadir}/doc/numix-icon-theme
cp -r numix-icon-theme-%{commit0}/Numix %{buildroot}%{_datadir}/icons/Numix
cp -r numix-icon-theme-%{commit0}/Numix-Light %{buildroot}%{_datadir}/icons/Numix-Light

mkdir -p %{buildroot}%{_datadir}/doc/numix-icon-theme-circle
cp -r numix-icon-theme-circle-%{commit1}/Numix-Circle %{buildroot}%{_datadir}/icons/Numix-Circle
cp -r numix-icon-theme-circle-%{commit1}/Numix-Circle-Light %{buildroot}%{_datadir}/icons/Numix-Circle-Light

# KDE Theme
mkdir -p %{buildroot}%{_datadir}/plasma/desktoptheme
mkdir -p %{buildroot}%{_datadir}/plasma/look-and-feel
mkdir -p %{buildroot}%{_datadir}/color-schemes
cp -r numix-kde-theme-%{commit3}/plasma/desktoptheme/Numix %{buildroot}%{_datadir}/plasma/desktoptheme/
cp -r numix-kde-theme-%{commit3}/plasma/look-and-feel/org.numixproject.kde %{buildroot}%{_datadir}/plasma/look-and-feel/
cp -r numix-kde-theme-%{commit3}/color-schemes/Numix.colors %{buildroot}%{_datadir}/color-schemes/

#### --- Create Numix-Blue Theme --- ####
cp -r numix-icon-theme-%{commit0}/Numix Numix-Blue
#Dark Blue Color
find ./Numix-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ef6c00/1570b8/g
#Light Blue Color
find ./Numix-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ffa726/52b1ef/g
#Folder logo color
find ./Numix-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/b17621/2383b6/g
#Smaller icons (16 and smaller)
find ./Numix-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ff9800/2383b6/g
# Replace Red (Preview and Numix Icons) with Blue
find ./Numix-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/dc322f/2383b6/g


sed -i s/Name=Numix/Name=Numix\ Blue/g ./Numix-Blue/index.theme
sed -i s/Project/Project\ with\ blue\ folders/g ./Numix-Blue/index.theme
cp -r Numix-Blue %{buildroot}%{_datadir}/icons/Numix-Blue

#### --- Create Numix-Circle-Blue Theme --- ####
cp -r  numix-icon-theme-circle-%{commit1}/Numix-Circle Numix-Circle-Blue
#Dark Blue Color
find ./Numix-Circle-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ef6c00/1570b8/g
#Light Blue Color
find ./Numix-Circle-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ffa726/52b1ef/g
#Folder logo color
find ./Numix-Circle-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/b17621/2383b6/g
#Smaller icons (16 and smaller)
find ./Numix-Circle-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/ff9800/2383b6/g
# Replace Red (Preview and Numix Icons) with Blue
find ./Numix-Circle-Blue -name "*.svg" -not -type l -print0 | xargs -0 sed -i s/dc322f/2383b6/g

sed -i s/Name=Numix\ Circle/Name=Numix\ Circle\ Blue/g ./Numix-Circle-Blue/index.theme
sed -i s/design/design\ and\ with\ blue\ folders\./g ./Numix-Circle-Blue/index.theme
sed -i s/Inherits=Numix/Inherits=Numix-Blue/g ./Numix-Circle-Blue/index.theme
cp -r Numix-Circle-Blue %{buildroot}%{_datadir}/icons/Numix-Circle-Blue

# This icon (Numix star-here) screws up Fedora Branding 
rm -rf %{buildroot}%{_datadir}/icons/Numix*/*/places/start-here*.svg
rm -rf %{buildroot}%{_datadir}/icons/Numix*/*/apps/start-here*.svg

%files icon-theme
%doc numix-icon-theme-%{commit0}/license numix-icon-theme-%{commit0}/readme.md
%{_datadir}/icons/Numix
%{_datadir}/icons/Numix-Light

%files icon-theme-blue
%doc numix-icon-theme-%{commit0}/license numix-icon-theme-%{commit0}/readme.md
%{_datadir}/icons/Numix-Blue

%files icon-theme-circle
%doc numix-icon-theme-circle-%{commit1}/LICENSE numix-icon-theme-circle-%{commit1}/README.md
%{_datadir}/icons/Numix-Circle
%{_datadir}/icons/Numix-Circle-Light

%files icon-theme-circle-blue
%doc numix-icon-theme-circle-%{commit1}/LICENSE numix-icon-theme-circle-%{commit1}/README.md
%{_datadir}/icons/Numix-Circle-Blue

%files gtk-theme
%doc numix-gtk-theme-%{commit2}/LICENSE numix-gtk-theme-%{commit2}/README.md numix-gtk-theme-%{commit2}/CREDITS
%{_datadir}/themes/Numix

%files kde-theme
%doc numix-kde-theme-%{commit3}/LICENSE numix-kde-theme-%{commit3}/README.md
%{_datadir}/plasma/look-and-feel/org.numixproject.kde
%{_datadir}/plasma/desktoptheme/Numix
%{_datadir}/color-schemes/Numix.colors

%changelog
* Tue Aug 22 2017 Ian Firns <firnsy@kororaproject.org> - 0.1.0-10
- Update to latest upstream commits

* Fri Nov 11 2016 Chris Smart <csmart@kororaproject.org> - 0.1.0-9
- Update to latest upstream commits

* Thu Oct 20 2016 Ian Firns <firnsy@kororaproject.org> - 0.1.0-8
- Update to latest upstream commits

* Mon Jul 11 2016 Chris Smart <csmart@kororaproject.org> - 0.1.0-7
- Fix the spec file
- Update to latest upstream commits, adds missing GNOME icons

* Sat Apr 16 2016 Sascha Spreitzer <sspreitz@redhat.com> - 0.1.0-6
- adjust groups and tabstops

* Sat Apr 16 2016 Sascha Spreitzer <sspreitz@redhat.com> - 0.1.0-5
- fix sources setup and relative dirs

* Sat Apr 16 2016 Sascha Spreitzer <sspreitz@redhat.com> - 0.1.0-4
- add license and readme files

* Sat Apr 16 2016 Sascha Spreitzer <sspreitz@redhat.com> - 0.1.0-3
- require gdk-pixbuf2

* Sat Apr 16 2016 Sascha Spreitzer <sspreitz@redhat.com> - 0.1.0-2
- refactor for git use

* Sun Jan 24 2016 Sascha Spreitzer <sspreitz@redhat.com>
- Refactor to build real srpms
* Tue Nov 10 2015 Sascha Spreitzer <sspreitz@redhat.com>
- Repackaging
- Adding Shine and uTouch
